### 使用vue3.compositionAPI重写cube-ui，适配vite.
> vite不再支持cjs打包模块。因此使用esmodule导出。

git地址：https://gitee.com/isfive/cubeui-next


项目依赖stylus，使用前请先安装stylus
```shell
npm install stylus -D
```
安装cubeui-next
```shell
npm install cubeui-next -S
```
```js
import cubeui from 'cubeui-next'

const app = createApp(App)
app.use(cubeui)
app.mount('#app')
```
组件列表（已玩成）
### 按钮-Cbutton
```html
<Cbutton>按钮</Cbutton>
```
### 加载-Cloading
```html
<Cloading />
```

