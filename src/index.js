import Cbutton from "./components/button/button.vue";
import Cloading from "./components/loading/loading.vue";
import "./icon/iconfont.css";
export { Cbutton, Cloading };
const components = [Cbutton, Cloading];
const install = (app) => {
  components.map((component) => {
    app.component(component.name, component);
  });
};

export default {
  install,
};
